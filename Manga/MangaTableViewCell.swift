//
//  MangaTableViewCell.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit
import AlamofireImage

class MangaTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource  {
    @IBOutlet weak var mangaCollectionView: UICollectionView!
    private var mangas = [Manga]()
    override func awakeFromNib() {
        super.awakeFromNib()
            mangaCollectionView.delegate = self
            mangaCollectionView.dataSource = self
            mangaCollectionView.reloadData()
        }
       
       func configure(item: [Manga]) {
         self.mangas = item
         mangaCollectionView.reloadData()
       }
       
       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return mangas.count
       }
   
    func prepareForSegue(for segue: UIStoryboardSegue, sender: Any?) {
             if segue.identifier == "MangaDetails" {
                func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                   let viewController = segue.destination as? DetailsViewController
//                let index = mangaCollectionView.indexPathsForSelectedItems
                    viewController!.cellValue = mangas[indexPath.item]
                }
          }
    }
    
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! MangaCollectionViewCell
             cell.poster.layer.cornerRadius = 20
             cell.titletext.text = mangas[indexPath.row].title
             cell.poster.af.setImage(withURL: (URL(string: "\(mangas[indexPath.row].imageUrl )") ?? nil)!)
             return cell
       }

}
