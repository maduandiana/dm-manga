//
//  MangaItem.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

class MangaItem: ListItem {
    var itemType: ItemType = ItemType.manga

    var items: [Manga]
    init(items: [Manga]) {
        self.items = items
    }
}
