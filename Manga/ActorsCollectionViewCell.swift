//
//  ActorsCollectionViewCell.swift
//  Manga
//
//  Created by Diana Maduan on 7/5/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

class ActorsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nametext: UITextView!
    @IBOutlet weak var poster: UIImageView!
   
}
