//
//  TopCollectionViewCell.swift
//  Manga
//
//  Created by Diana Maduan on 6/5/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

class TopCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var namelabel: UITextView!
    @IBOutlet weak var poster: UIImageView!
  
}
