//
//  LocalDataSource.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

class LocalDataSource: NSObject {
    
    func cashActionGenres(mangas: [Manga]){
        UserDefaults.standard.set(try? PropertyListEncoder().encode(mangas), forKey: "actionManga")
                        // UserDefaults.standard.removeObject(forKey: "facts")
    }
       func getActionGenre(completion: @escaping ([Manga]) -> Void)
       {
           if let data = UserDefaults.standard.value(forKey: "actionManga") as? Data {
                  let mangas = try! PropertyListDecoder().decode(Array<Manga>.self, from: data)
                   completion(mangas)
           }
       
       }
    func cashCharacters(actors: [Actors]){
         UserDefaults.standard.set(try? PropertyListEncoder().encode(actors), forKey: "actors")
                         // UserDefaults.standard.removeObject(forKey: "facts")
     }
        func getCharacters(completion: @escaping ([Actors]) -> Void)
        {
            if let data = UserDefaults.standard.value(forKey: "actors") as? Data {
                   let actors = try! PropertyListDecoder().decode(Array<Actors>.self, from: data)
                    completion(actors)
            }
        
        }
     func cashRecommendations(actors: [Recommendations]){
            UserDefaults.standard.set(try? PropertyListEncoder().encode(actors), forKey: "recommendations")
                            // UserDefaults.standard.removeObject(forKey: "facts")
        }
           func getRecommendations(completion: @escaping ([Recommendations]) -> Void)
           {
               if let data = UserDefaults.standard.value(forKey: "recommendations") as? Data {
                      let actors = try! PropertyListDecoder().decode(Array<Recommendations>.self, from: data)
                       completion(actors)
               }
           
           }
    func cashAdventureGenres(mangas: [Manga]){
        UserDefaults.standard.set(try? PropertyListEncoder().encode(mangas), forKey: "adventureManga")
                        // UserDefaults.standard.removeObject(forKey: "facts")
    }
       func getAdventureGenre(completion: @escaping ([Manga]) -> Void)
       {
           if let data = UserDefaults.standard.value(forKey: "adventureManga") as? Data {
                  let mangas = try! PropertyListDecoder().decode(Array<Manga>.self, from: data)
                   completion(mangas)
           }
       
       }
    
    func cashCarsGenres(mangas: [Manga]){
        UserDefaults.standard.set(try? PropertyListEncoder().encode(mangas), forKey: "carsManga")
                        // UserDefaults.standard.removeObject(forKey: "facts")
    }
       func getCarsGenre(completion: @escaping ([Manga]) -> Void)
       {
           if let data = UserDefaults.standard.value(forKey: "carsManga") as? Data {
                  let mangas = try! PropertyListDecoder().decode(Array<Manga>.self, from: data)
                   completion(mangas)
           }
       
       }
    
    func cashDementiaGenres(mangas: [Manga]){
        UserDefaults.standard.set(try? PropertyListEncoder().encode(mangas), forKey: "dementiaManga")
                        // UserDefaults.standard.removeObject(forKey: "facts")
    }
       func getDementiaGenre(completion: @escaping ([Manga]) -> Void)
       {
           if let data = UserDefaults.standard.value(forKey: "dementiaManga") as? Data {
                  let mangas = try! PropertyListDecoder().decode(Array<Manga>.self, from: data)
                   completion(mangas)
           }
       
       }
    
    func cashDemonsGenres(mangas: [Manga]){
           UserDefaults.standard.set(try? PropertyListEncoder().encode(mangas), forKey: "demonsManga")
                           // UserDefaults.standard.removeObject(forKey: "facts")
       }
          func getDemonsGenre(completion: @escaping ([Manga]) -> Void)
          {
              if let data = UserDefaults.standard.value(forKey: "demonsManga") as? Data {
                     let mangas = try! PropertyListDecoder().decode(Array<Manga>.self, from: data)
                      completion(mangas)
              }
          
          }
    
    func cashDramaGenres(mangas: [Manga]){
              UserDefaults.standard.set(try? PropertyListEncoder().encode(mangas), forKey: "dramaManga")
                              // UserDefaults.standard.removeObject(forKey: "facts")
          }
             func getDramaGenre(completion: @escaping ([Manga]) -> Void)
             {
                 if let data = UserDefaults.standard.value(forKey: "dramaManga") as? Data {
                        let mangas = try! PropertyListDecoder().decode(Array<Manga>.self, from: data)
                         completion(mangas)
                 }
             
             }
    
    func cashComedyGenres(mangas: [Manga] ){
              UserDefaults.standard.set(try? PropertyListEncoder().encode(mangas), forKey: "comedyManga")
                              // UserDefaults.standard.removeObject(forKey: "facts")
          }
             func getComedyGenre(completion: @escaping ([Manga]) -> Void)
             {
                 if let data = UserDefaults.standard.value(forKey: "comedyManga") as? Data {
                        let mangas = try! PropertyListDecoder().decode(Array<Manga>.self, from: data)
                         completion(mangas)
                 }
             
             }

    
    func cashTop(mangas: [TopManga]){
              UserDefaults.standard.set(try? PropertyListEncoder().encode(mangas), forKey: "topManga")
                              // UserDefaults.standard.removeObject(forKey: "facts")
          }
             func getTopManga(completion: @escaping ([TopManga]) -> Void)
             {
                 if let data = UserDefaults.standard.value(forKey: "topManga") as? Data {
                        let mangas = try! PropertyListDecoder().decode(Array<TopManga>.self, from: data)
                         completion(mangas)
                 }
             
             }
}
