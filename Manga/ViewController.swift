//
//  ViewController.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit
import FirebaseAuth
class ViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    var recommendations = [Recommendations]()
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var posterView: UIImageView!
    var listItems = [ListItem]()
    var manga1 = [Manga]()
    var manga2 = [Manga]()
    var manga3 = [Manga]()
    var manga4 = [Manga]()
    var manga5 = [Manga]()
    var manga6 = [Manga]()
    var manga7 = [Manga]()
    var manga8 = [Manga]()
    
    var topmanga = [TopManga]()
    var actors = [Actors]()
    private var viewmodel = ViewModel(repo: Repository(remoteDS: RemoteDataSource(service: ApiManager()), localDS: LocalDataSource()))
    override func viewDidLoad() {
        super.viewDidLoad()
        posterView.layer.cornerRadius = 20
        viewmodel.loadActionGenre(completion: {
                                    (response) in
                               self.manga1 = response
                               self.tableview.reloadData()
                    })
        viewmodel.loadAdventureGenre(completion: {
                                           (response) in
                               self.manga2 = response
                              self.tableview.reloadData()
                           })
        viewmodel.loadCarsGenre(completion: {
                                           (response) in
                               self.manga3 = response
            print("cars",response)
                               self.tableview.reloadData()
                           })
        viewmodel.loadDramaGenre(completion: {
                                           (response) in
                               self.manga4 = response
                               self.tableview.reloadData()
                           })
        viewmodel.loadDementiaGenre(completion: {
                                           (response) in
                               self.manga5 = response
            print("dementia",response)
                               self.tableview.reloadData()
                           })
        viewmodel.loadDemonsGenre(completion: {
                                           (response) in
                               self.manga6 = response
                              self.tableview.reloadData()
                           })

        viewmodel.loadComedyGenre(completion: {
                                           (response) in
                               self.manga7 = response
                               self.tableview.reloadData()
                           })
            listItems.append(GenreItem(label: "Action Manga"))
            listItems.append(MangaItem(items: manga1))
            listItems.append(GenreItem(label: "Adventure Manga"))
            listItems.append(MangaItem(items: manga2))
            listItems.append(GenreItem(label: "Drama Manga"))
            listItems.append(MangaItem(items: manga4))
            listItems.append(GenreItem(label: "Demons Manga"))
            listItems.append(MangaItem(items: manga6))
            listItems.append(GenreItem(label: "Comedy Manga"))
            listItems.append(MangaItem(items: manga7))
            listItems.append(GenreItem(label: "Dementia Manga"))
            listItems.append(MangaItem(items: manga5))
            
            listItems.append(GenreItem(label: "Cars Manga"))
            listItems.append(MangaItem(items: manga3))
            
          
            tableview.delegate = self
            tableview.dataSource = self
            tableview.reloadData()
         }
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return listItems.count
        
      }
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let item = listItems[indexPath.row]
          let cell = tableView.dequeueReusableCell(withIdentifier: "MangaCell", for: indexPath) as! MangaTableViewCell
       
        if item.itemType == .genre {
              let cell = tableView.dequeueReusableCell(withIdentifier: "GenreCell", for: indexPath) as! GenreTableViewCell
              cell.configure(item: item as! GenreItem)
            
              return cell
        }
         if indexPath.row == 1 {
            cell.configure(item: manga1)
            return cell
        }
         if indexPath.row == 3 {
            cell.configure(item: manga2)
            return cell
            }
        if indexPath.row == 5 {
            cell.configure(item: manga4)
            return cell
            }
        if indexPath.row == 7 {
            cell.configure(item: manga6)
            return cell
        }
        if indexPath.row == 9 {
                 cell.configure(item: manga7)
                 return cell
             }
        if indexPath.row == 11 {
                 cell.configure(item: manga6)
                 return cell
             }
        if indexPath.row == 13 {
                 cell.configure(item: manga4)
                 return cell
             }
       // cell.cellDelegate = self as! CollectionViewCellDelegate
        return cell
    }
            
    @IBAction func openTop(_ sender: Any) {
        let id = "TopManga"
        let FAVViewController = storyboard?.instantiateViewController(identifier: id)
        as? TopViewController
        view.window?.rootViewController = FAVViewController
        view.window?.makeKeyAndVisible()
          
    }
    
    @IBAction func openSearch(_ sender: Any) {
        let id = "SearchManga"
        let FAVViewController = storyboard?.instantiateViewController(identifier: id)
        as? SearchMangaViewController
        view.window?.rootViewController = FAVViewController
        view.window?.makeKeyAndVisible()
    }
    @IBAction func openProfile(_ sender: Any) {
        Auth.auth().addStateDidChangeListener() { auth, user in
                      if user != nil {
                          self.transitionToFavouritesPage()
                      }else{
                       self.transitionToRegisterPage()
               }
                  }
    }

    func transitionToRegisterPage(){
       let id = "AuthVC"
       let FAVViewController = storyboard?.instantiateViewController(identifier: id)
       as? AuthViewController
       view.window?.rootViewController = FAVViewController
       view.window?.makeKeyAndVisible()
         
    }
    func transitionToFavouritesPage(){
       let id = "ProfileVC"
       let FAVViewController = storyboard?.instantiateViewController(identifier: id)
       as? ProfileViewController
       view.window?.rootViewController = FAVViewController
       view.window?.makeKeyAndVisible()
       }
}

