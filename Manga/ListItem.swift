//
//  ListItem.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit


protocol ListItem {
    var itemType: ItemType { get }
}

enum ItemType {
    case genre, manga
}
