//
//  ProfileViewController.swift
//  Manga
//
//  Created by Diana Maduan on 8/5/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit
import FirebaseAuth
class ProfileViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    private let localmanager = CoreDataManager()
    @IBOutlet weak var tableView: UITableView!
    var mangas = [Mangas]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mangas = self.localmanager.getList()
     //   localmanager.deleteallfromList()
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return mangas.count
       }
       
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "MyManga", for: indexPath) as! MyMangaTableViewCell
                   cell.titlelabel.text = mangas[indexPath.row].title
                   cell.scorelabel.text = String(mangas[indexPath.row].score)
                   cell.poster.layer.cornerRadius = 20
    cell.poster.af.setImage(withURL: (URL(string: "\(mangas[indexPath.row].image ?? "nil" )") ?? nil)!)
                   return cell
   }
       

    @IBAction func logout(_ sender: Any) {
        let id = "HomePage"
        let FAVViewController = storyboard?.instantiateViewController(identifier: id)
        as? ViewController
        view.window?.rootViewController = FAVViewController
        view.window?.makeKeyAndVisible()
        try! Auth.auth().signOut()
    }
    @IBAction func search(_ sender: Any) {
       let id = "SearchManga"
       let FAVViewController = storyboard?.instantiateViewController(identifier: id)
       as? SearchMangaViewController
       view.window?.rootViewController = FAVViewController
       view.window?.makeKeyAndVisible()
    }
    
    @IBAction func backHome(_ sender: Any) {
        let id = "HomePage"
        let FAVViewController = storyboard?.instantiateViewController(identifier: id)
        as? ViewController
        view.window?.rootViewController = FAVViewController
        view.window?.makeKeyAndVisible()
    }
}
