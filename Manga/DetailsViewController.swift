//
//  DetailsViewController.swift
//  Manga
//
//  Created by Diana Maduan on 30/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import FirebaseAuth
import CoreData
class DetailsViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource{
    private let localmanager = CoreDataManager()
    var cellValue : Manga? = nil
    private var mangaid = 1
    private var viewmodel = ViewModel(repo: Repository(remoteDS: RemoteDataSource(service: ApiManager()), localDS: LocalDataSource()))
    var actors = [Actors]()
    var recommendations = [Recommendations]()
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titlelabel: UITextView!
    @IBOutlet weak var members: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var synopsis: UITextView!
    @IBOutlet weak var scorelabel: UILabel!
    @IBOutlet weak var chapters: UILabel!
    @IBOutlet weak var addtolist: UIButton!
    @IBOutlet weak var poster: UIImageView!
    
    override func viewDidLoad() {
            super.viewDidLoad()
            poster.layer.cornerRadius = 30
            addtolist.layer.cornerRadius = 20
            if cellValue != nil {
                mangaid = (cellValue?.id ?? nil)!
                titlelabel.text = cellValue?.title
                scorelabel.text = (String(describing: cellValue!.score))
                chapters.text = "32"
                members.text = (String(describing: cellValue!.members))
                synopsis.text = cellValue?.synopsis
                poster.af.setImage(withURL: (URL(string: "\(cellValue!.imageUrl)") ?? nil)! )
        }
           viewmodel.loadCharacters(id:mangaid,completion: {
                                                              (response) in
                                                     self.actors = response
                                                     self.collectionView.reloadData()
                                                    
                                                 })
                     collectionView.delegate = self
                     collectionView.dataSource = self
                     collectionView.reloadData()
        
    }

 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return actors.count
     }

      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActorsCell", for: indexPath) as! ActorsCollectionViewCell
           cell.poster.layer.cornerRadius = 20
           cell.nametext.text = actors[indexPath.row].name
           cell.poster.af.setImage(withURL: (URL(string: "\(actors[indexPath.row].image )")!)) 
          return cell
         }
    
    @IBAction func addloListaction(_ sender: Any) {
        Auth.auth().addStateDidChangeListener() { auth, user in
                             if user != nil {
                                self.localmanager.addToList(id: self.cellValue!.id, title: self.cellValue!.title, synopsis: self.cellValue!.synopsis, score: self.cellValue!.score, user: user!.providerID, image:self.cellValue!.imageUrl, members:self.cellValue!.members)
                               let alert = UIAlertController(title: "Alert", message: "Added to favourites", preferredStyle: UIAlertController.Style.alert)
                               alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                               self.present(alert, animated: true, completion: nil)
                             } else {
                              self.transitionToRegisterPage()
                      }
        }
    }
    
    func transitionToRegisterPage(){
         let id = "AuthVC"
         let authViewController = storyboard?.instantiateViewController(identifier: id)
         as? AuthViewController
         view.window?.rootViewController = authViewController
         view.window?.makeKeyAndVisible()
    }
    @IBAction func backhome(_ sender: Any) {
        let id = "HomePage"
        let FAVViewController = storyboard?.instantiateViewController(identifier: id)
        as? ViewController
        view.window?.rootViewController = FAVViewController
        view.window?.makeKeyAndVisible()
    }
}
