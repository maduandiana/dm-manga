//
//  CoreDataManager.swift
//  Manga
//
//  Created by Diana Maduan on 1/5/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit
import CoreData
class CoreDataManager {
       private func getAppDelegate()->AppDelegate{
               return UIApplication.shared.delegate as! AppDelegate
          }
       private func getManagedContext()-> NSManagedObjectContext{
              return getAppDelegate().persistentContainer.viewContext
          }

       private func getMangaEntity()-> NSEntityDescription{
                 return  NSEntityDescription.entity(forEntityName: "Mangas", in: getManagedContext())!
             }

       private func getFetchRequest()-> NSFetchRequest<NSManagedObject>{
                    return  NSFetchRequest<NSManagedObject>(entityName: "Mangas")
                }
       
    func addToList(id:Int,title:String,synopsis:String,score:Float, user:String,image:String,members:Int){
              let manga = NSManagedObject(entity: getMangaEntity(), insertInto: getManagedContext())
              manga.setValue(id, forKeyPath: "id")
              manga.setValue(title, forKeyPath: "title")
              manga.setValue(synopsis, forKeyPath: "synopsis")
              manga.setValue(user, forKeyPath: "user")
              manga.setValue(image, forKeyPath: "image")
              manga.setValue(score, forKeyPath: "score")
              manga.setValue(members, forKeyPath: "members")
              do{
                        try getManagedContext().save()
                     } catch let error as NSError{
                         print("\(error.userInfo)")
                     }
         }
       
    func getList()->[Mangas]{
                 do{
                       let mangas = try getManagedContext().fetch(getFetchRequest()) as! [Mangas]
//                       for manga in mangas{
//                        if manga.value(forKeyPath: "user") == user {
//                            print(manga.value(forKeyPath: "title") as! String)
//                       }
//                    }
            return try (getManagedContext().fetch(getFetchRequest()) as! [Mangas])
                   } catch let error as NSError{
                       print("\(error.userInfo)")
                   }
                 return []
             }

       func deletefromList(id:Int){
                 do{
                    let request = getFetchRequest()
                    request.predicate = NSPredicate(format: "id = %i", id)
                    let manga = try getManagedContext().fetch(getFetchRequest()) as! [Mangas]
                     if manga.isEmpty{
                         print("manga id not found")
                         return
                     }
                     let mang = manga[0]
                     getManagedContext().delete(mang)
                     try getManagedContext().save()
                   } catch let error as NSError{
                       print("\(error.userInfo)")
                   }

             }

      func deleteallfromList(){
                     do{
                        let request = getFetchRequest()
                        request.includesPropertyValues = false
                        let movies = try getManagedContext().fetch(getFetchRequest()) as! [Mangas]
                         if movies.isEmpty{
                             print("manga id not found")
                             return
                         }
                         for movie in movies{
                         getManagedContext().delete(movie)
                         }
                         try getManagedContext().save()
                       } catch let error as NSError{
                           print("\(error.userInfo)")
                       }

                 }
    
    
}
