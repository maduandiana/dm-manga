//
//  TopMangaTableViewCell.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit
import AlamofireImage
class TopMangaTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource  {

    @IBOutlet weak var mangaCollectioView: UICollectionView!
    
        private var mangas = [Manga]()
        override func awakeFromNib() {
            super.awakeFromNib()
                mangaCollectioView.delegate = self
                mangaCollectioView.dataSource = self
                mangaCollectioView.reloadData()
            }
           
           func configure(item: MangaItem) {
            self.mangas = item.items
           }
           
           func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
               return mangas.count
           }
           
           func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
               
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopColCell", for: indexPath) as! TopCollectionViewCell
                cell.titlelabel.text = mangas[indexPath.row].title
                cell.poster.af.setImage(withURL: (URL(string: "\(mangas[indexPath.row].imageUrl )") ?? nil)!)
                   return cell
           }

    }
