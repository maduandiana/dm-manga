//
//  GenreItem.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

class GenreItem: ListItem {
    var itemType: ItemType = ItemType.genre
    
    var label: String!
    
    init(label: String) {
        self.label = label
    }
}
