//
//  DataResponse.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

class Actors:Codable{
    let id: Int
    let name: String
    let image: String
    init(id: Int,name:String,image: String){
        self.id = id
        self.name = name
        self.image = image
    }
}

extension Actors{
    enum CodingKeys: String, CodingKey {
        case id = "mal_id"
        case name = "name"
        case image = "image_url"
    }
}
class CharacterResponse:Codable{
    let characters: [Actors]
    init(characters: [Actors]){
        self.characters = characters
    }
}

class Manga:Codable{
    let id: Int
    let title: String
    let imageUrl: String
    let synopsis: String
    let score: Float
    let members: Int
    init(id: Int,title:String,imageUrl: String,synopsis: String,score: Float,members: Int){
        self.id = id
        self.title = title
        self.imageUrl = imageUrl
        self.score = score
        self.synopsis = synopsis
        self.members = members
    }
}

extension Manga{
    enum CodingKeys: String, CodingKey {
        case id = "mal_id"
        case title = "title"
        case imageUrl = "image_url"
        case synopsis
        case score
        case members
    }
}
class SearchResponse:Codable{
    let results: [Manga]
    init(results: [Manga]){
        self.results = results
    }
}


//1
class ActionGenreResponse:Codable{
    let manga: [Manga]
    init(manga: [Manga]){
        self.manga = manga
    }
}

//2
class AdventureGenreResponse:Codable{
    let manga: [Manga]
    init(manga: [Manga]){
        self.manga = manga
    }
}

//3
class CarsGenreResponse:Codable{
    let manga: [Manga]
    init(manga: [Manga]){
        self.manga = manga
    }
}

//4
class ComedyGenreResponse:Codable{
    let manga: [Manga]
    init(manga: [Manga]){
        self.manga = manga
    }
}

//5
class DementiaGenreResponse:Codable{
    let manga: [Manga]
    init(manga: [Manga]){
        self.manga = manga
    }
}


//6
class DemonsGenreResponse:Codable{
    let manga: [Manga]
    init(manga: [Manga]){
        self.manga = manga
    }
}


//7
class DramaGenreResponse:Codable{
    let manga: [Manga]
    init(manga: [Manga]){
        self.manga = manga
    }
}


 

class TopManga:Codable{
    let id: Int
    let title: String
    let image: String
    let score: Float
    init(id: Int,title:String,image: String,score: Float){
        self.id = id
        self.title = title
        self.image = image
        self.score = score
    }
}

extension TopManga{
    enum CodingKeys: String, CodingKey {
        case id = "mal_id"
        case title = "title"
        case image = "image_url"
        case score
    }
}
class TopResponse:Codable{
    let top: [TopManga]
    init(top: [TopManga]){
        self.top = top
    }
}

class Recommendations:Codable{
    let id: Int
    let title: String
    let image: String
    init(id: Int,title:String,image: String){
        self.id = id
        self.title = title
        self.image = image
    }
}

extension Recommendations{
    enum CodingKeys: String, CodingKey {
        case id = "mal_id"
        case title = "title"
        case image = "image_url"
    }
}
class RecommendationsResponse:Codable{
    let recommendations: [Recommendations]
    init(recommendations: [Recommendations]){
        self.recommendations = recommendations
    }
}

