//
//  RecomendationsCollectionViewCell.swift
//  Manga
//
//  Created by Diana Maduan on 7/5/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

class RecomendationsCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var titlelabel: UITextView!
}
