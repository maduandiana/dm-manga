//
//  ApiManager.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

import Alamofire

class ApiManager {
    private let endpoint = "https://api.jikan.moe/v3/"
    private let searchenpoint = "https://api.jikan.moe/v3/search/manga?q="
    private let charenpoint = "https://api.jikan.moe/v3/manga/"
      
    func getSearchResults(text:String,onComplete: @escaping (SearchResponse) -> Void, onError: @escaping()-> Void){
             AF.request("\(searchenpoint)\(text)&page=1").responseData{(response) in
             let resultResponse = try? JSONDecoder().decode(SearchResponse.self, from: response.data!)
                 if resultResponse == nil {
                     onError()
                     return
                 }
                  onComplete(resultResponse!)
             }
         }
    func getCharacters(id:Int,onComplete: @escaping (CharacterResponse) -> Void, onError: @escaping()-> Void){
                AF.request("\(charenpoint)\(id)/characters").responseData{(response) in
                let resultResponse = try? JSONDecoder().decode(CharacterResponse.self, from: response.data!)
                    if resultResponse == nil {
                        onError()
                        return
                    }
                     onComplete(resultResponse!)
                }
            }
      func getRecommendations(id:Int,onComplete: @escaping (RecommendationsResponse) -> Void, onError: @escaping()-> Void){
                AF.request("\(charenpoint)\(id)/recommendations").responseData{(response) in
                let resultResponse = try? JSONDecoder().decode(RecommendationsResponse.self, from: response.data!)
                    if resultResponse == nil {
                        onError()
                        print("bfhcdc")
                        return
                    }
                     onComplete(resultResponse!)
                    print(resultResponse!)
                }
            }
       func getActionGenre(onComplete: @escaping (ActionGenreResponse) -> Void, onError: @escaping()-> Void){
           AF.request("https://api.jikan.moe/v3/genre/manga/1").responseData{(response) in
           let genreResponse = try? JSONDecoder().decode(ActionGenreResponse.self, from: response.data!)
               if genreResponse == nil {
                   onError()
                   return
               }
                onComplete(genreResponse!)
            
           }
       }
    func getAdventureGenre(onComplete: @escaping (AdventureGenreResponse) -> Void, onError: @escaping()-> Void){
             AF.request("https://api.jikan.moe/v3/genre/manga/2").responseData{(response) in
             let genreResponse = try? JSONDecoder().decode(AdventureGenreResponse.self, from: response.data!)
                 if genreResponse == nil {
                     onError()
                     return
                 }
                  onComplete(genreResponse!)
             }
         }
    func getCarsGenre(onComplete: @escaping (CarsGenreResponse) -> Void, onError: @escaping()-> Void){
                AF.request("https://api.jikan.moe/v3/genre/manga/3").responseData{(response) in
                let genreResponse = try? JSONDecoder().decode(CarsGenreResponse.self, from: response.data!)
                    if genreResponse == nil {
                        onError()
                        return
                    }
                     onComplete(genreResponse!)
                }
            }
    func getComedyGenre(onComplete: @escaping (ComedyGenreResponse) -> Void, onError: @escaping()-> Void){
                  AF.request("https://api.jikan.moe/v3/genre/manga/4").responseData{(response) in
                  let genreResponse = try? JSONDecoder().decode(ComedyGenreResponse.self, from: response.data!)
                      if genreResponse == nil {
                          onError()
                          return
                      }
                       onComplete(genreResponse!)
                  }
              }
    func getDementiaGenre(onComplete: @escaping (DementiaGenreResponse) -> Void, onError: @escaping()-> Void){
                   AF.request("https://api.jikan.moe/v3/genre/manga/5").responseData{(response) in
                   let genreResponse = try? JSONDecoder().decode(DementiaGenreResponse.self, from: response.data!)
                       if genreResponse == nil {
                           onError()
                           return
                       }
                        onComplete(genreResponse!)
                   }
               }
    func getDemonsGenre(onComplete: @escaping (DemonsGenreResponse) -> Void, onError: @escaping()-> Void){
                   AF.request("https://api.jikan.moe/v3/genre/manga/6").responseData{(response) in
                   let genreResponse = try? JSONDecoder().decode(DemonsGenreResponse.self, from: response.data!)
                       if genreResponse == nil {
                           onError()
                           return
                       }
                        onComplete(genreResponse!)
                   }
               }

    func getDramaGenre(onComplete: @escaping (DramaGenreResponse) -> Void, onError: @escaping()-> Void){
                      AF.request("https://api.jikan.moe/v3/genre/manga/7").responseData{(response) in
                      let genreResponse = try? JSONDecoder().decode(DramaGenreResponse.self, from: response.data!)
                          if genreResponse == nil {
                              onError()
                              return
                          }
                           onComplete(genreResponse!)
                      }
                  }
    func getTopManga(onComplete: @escaping (TopResponse) -> Void, onError: @escaping()-> Void){
                        AF.request("\(endpoint)top/manga").responseData{(response) in
                        let genreResponse = try? JSONDecoder().decode(TopResponse.self, from: response.data!)
                            if genreResponse == nil {
                                onError()
                                return
                            }
                             onComplete(genreResponse!)
                        }
                    }
}
