//
//  GenreTableViewCell.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

class GenreTableViewCell: UITableViewCell {

   
    @IBOutlet weak var genrelabel: UILabel!
    func configure(item: GenreItem) {
              genrelabel.text = item.label
          }
}
