//
//  AuthViewController.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController {

    @IBOutlet weak var signupbtn: UIButton!
    @IBOutlet weak var loginbtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        loginbtn.layer.cornerRadius = 20
        signupbtn.layer.cornerRadius = 20
        loginbtn.layer.borderWidth = 1
        loginbtn.layer.borderColor = UIColor.purple.cgColor
        signupbtn.layer.borderWidth = 1
        signupbtn.layer.borderColor = UIColor.systemPink.cgColor
    }
    

    @IBAction func SignInPage(_ sender: Any) {
        let id = "SignVC"
        let signViewController = storyboard?.instantiateViewController(identifier: id)
        as? SignViewController
        view.window?.rootViewController = signViewController
        view.window?.makeKeyAndVisible()
    }
    
    @IBAction func loginPage(_ sender: Any) {
        let id = "LoginVC"
        let loginViewController = storyboard?.instantiateViewController(identifier: id)
        as? LogViewController
        view.window?.rootViewController = loginViewController
        view.window?.makeKeyAndVisible()
    }
}
