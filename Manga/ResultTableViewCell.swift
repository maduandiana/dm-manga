//
//  ResultTableViewCell.swift
//  Manga
//
//  Created by Diana Maduan on 30/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

class ResultTableViewCell: UITableViewCell {


    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var poster: UIImageView!
}
