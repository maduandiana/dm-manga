//
//  SignViewController.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit
import FirebaseAuth
class SignViewController: UIViewController {

    @IBOutlet weak var errorlabel: UILabel!
    @IBOutlet weak var signbtn: UIButton!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        signbtn.layer.cornerRadius = 20
        signbtn.layer.borderWidth = 1
        signbtn.layer.borderColor = UIColor.purple.cgColor
        password.isSecureTextEntry = true
    }
    
    @IBAction func openLoginPage(_ sender: Any) {
        let id = "LoginVC"
        let loginViewController = storyboard?.instantiateViewController(identifier: id)
        as? LogViewController
        view.window?.rootViewController = loginViewController
        view.window?.makeKeyAndVisible()
    }
    
    @IBAction func signup(_ sender: Any) {
        let email = username.text!.trimmingCharacters(in: .whitespacesAndNewlines)
               let passwords = password.text!.trimmingCharacters(in: .whitespacesAndNewlines)
               Auth.auth().createUser(withEmail: email, password: passwords){
                          (result,error) in
                          if error != nil{
                              self.errorlabel.text = error!.localizedDescription
                              return
                          }
                       self.transitionToLoginPage()
                      }
    }
    func transitionToLoginPage(){
           let id = "LoginVC"
           let loginViewController = storyboard?.instantiateViewController(identifier: id)
           as? LogViewController
           view.window?.rootViewController = loginViewController
           view.window?.makeKeyAndVisible()
          }
}
