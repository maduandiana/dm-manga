//
//  Repository.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit


class Repository {
    private var remoteDataSource: RemoteDataSource!
       private var localDataSource: LocalDataSource!
       
       init(remoteDS: RemoteDataSource, localDS: LocalDataSource){
           self.remoteDataSource = remoteDS
           self.localDataSource = localDS
       }
      func getSearchResults(text: String,completion: @escaping (SearchResponse) -> Void){
        remoteDataSource.getSearchResults(text: text){
                                      (response) in
                                      if response != nil {
                                          completion(response!)
                                      }
            
         }
    }
       func getActionGenre(onComplete: @escaping (ActionGenreResponse) -> Void){
               remoteDataSource.getActionGenre{
                   (response) in
                   if response != nil {
                    self.localDataSource.cashActionGenres(mangas: response!.manga)
                       onComplete(response!)
                   } else {
                       self.localDataSource.getActionGenre{ (mangas) in
                        onComplete(ActionGenreResponse(manga: mangas))
                       }
                   }
               }
       }
    func getCharacters(id:Int,onComplete: @escaping (CharacterResponse) -> Void){
        remoteDataSource.getCharacters(id: id){
                (response) in
                if response != nil {
                 self.localDataSource.cashCharacters(actors: response!.characters)
                    onComplete(response!)
                } else {
                    self.localDataSource.getCharacters{ (actors) in
                        onComplete(CharacterResponse(characters: actors ))
                    }
                }
            }
    }
    func getRecommendations(id:Int,onComplete: @escaping (RecommendationsResponse) -> Void){
          remoteDataSource.getRecommendations(id: id){
                  (response) in
                  if response != nil {
                   self.localDataSource.cashRecommendations(actors: response!.recommendations)
                      onComplete(response!)
                  } else {
                      self.localDataSource.getRecommendations{ (actors) in
                          onComplete(RecommendationsResponse(recommendations: actors ))
                      }
                  }
              }
      }
    func getAdventureGenre(onComplete: @escaping (AdventureGenreResponse) -> Void){
                remoteDataSource.getAdventureGenre{
                    (response) in
                    if response != nil {
                     self.localDataSource.cashAdventureGenres(mangas: response!.manga)
                        onComplete(response!)
                    } else {
                        self.localDataSource.getAdventureGenre{ (mangas) in
                         onComplete(AdventureGenreResponse(manga: mangas))
                        }
                    }
                }
        }
    func getCarsGenre(onComplete: @escaping (CarsGenreResponse) -> Void){
                  remoteDataSource.getCarsGenre{
                      (response) in
                      if response != nil {
                       self.localDataSource.cashCarsGenres(mangas: response!.manga)
                          onComplete(response!)
                      } else {
                          self.localDataSource.getCarsGenre{ (mangas) in
                           onComplete(CarsGenreResponse(manga: mangas ))
                          }
                      }
                  }
          }
    func getDementiaGenre(onComplete: @escaping (DementiaGenreResponse) -> Void){
                   remoteDataSource.getDementiaGenre{
                       (response) in
                       if response != nil {
                        self.localDataSource.cashDementiaGenres(mangas: response!.manga)
                           onComplete(response!)
                       } else {
                           self.localDataSource.getDementiaGenre{ (mangas) in
                            onComplete(DementiaGenreResponse(manga: mangas ))
                           }
                       }
                   }
           }
    func getDemonsGenre(onComplete: @escaping (DemonsGenreResponse) -> Void){
                    remoteDataSource.getDemonsGenre{
                        (response) in
                        if response != nil {
                         self.localDataSource.cashDementiaGenres(mangas: response!.manga)
                            onComplete(response!)
                        } else {
                            self.localDataSource.getDemonsGenre{ (mangas) in
                             onComplete(DemonsGenreResponse(manga: mangas ))
                            }
                        }
                    }
            }
     func getComedyGenre(onComplete: @escaping (ComedyGenreResponse) -> Void){
                        remoteDataSource.getComedyGenre{
                            (response) in
                            if response != nil {
                             self.localDataSource.cashComedyGenres(mangas: response!.manga)
                                onComplete(response!)
                            } else {
                                self.localDataSource.getComedyGenre{ (mangas) in
                                 onComplete(ComedyGenreResponse(manga: mangas))
                                }
                            }
                        }
                }
    func getDramaGenre(onComplete: @escaping (DramaGenreResponse) -> Void){
                          remoteDataSource.getDramaGenre{
                              (response) in
                              if response != nil {
                               self.localDataSource.cashDramaGenres(mangas: response!.manga)
                                  onComplete(response!)
                              } else {
                                  self.localDataSource.getDramaGenre{ (mangas) in
                                   onComplete(DramaGenreResponse(manga: mangas ))
                                  }
                              }
                          }
                  }
    func getTopManga(onComplete: @escaping (TopResponse) -> Void){
                          remoteDataSource.getTopManga{
                              (response) in
                              if response != nil {
                                self.localDataSource.cashTop(mangas: response!.top)
                                  onComplete(response!)
                              } else {
                                  self.localDataSource.getTopManga{ (mangas) in
                                   onComplete(TopResponse(top: mangas))
                                  }
                              }
                          }
                  }
    
}
