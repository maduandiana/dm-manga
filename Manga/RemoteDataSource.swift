//
//  RemoteDataSource.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

class RemoteDataSource {
    private var service: ApiManager
    init(service: ApiManager) {
       self.service = service
    }
    func getSearchResults(text:String,completion: @escaping (SearchResponse?) -> Void){
              if Connectivity.isConnectedToInternet() {
                service.getSearchResults(text:text,onComplete: {
                             (response) in
                             completion(response)
                         }){
                             
                         }
              } else {
                  completion(nil)
              }
             
          }
    func getCharacters(id:Int,completion: @escaping (CharacterResponse?) -> Void){
                 if Connectivity.isConnectedToInternet() {
                   service.getCharacters(id:id,onComplete: {
                                (response) in
                                completion(response)
                            }){
                                
                            }
                 } else {
                     completion(nil)
                 }
                
             }
      func getRecommendations(id:Int,completion: @escaping (RecommendationsResponse?) -> Void){
                 if Connectivity.isConnectedToInternet() {
                   service.getRecommendations(id:id,onComplete: {
                                (response) in
                                completion(response)
                            }){
                                
                            }
                 } else {
                     completion(nil)
                 }
                
             }
       func getActionGenre(completion: @escaping (ActionGenreResponse?) -> Void){
           if Connectivity.isConnectedToInternet() {
               service.getActionGenre(onComplete: {
                          (response) in
                          completion(response)
                      }){
                          
                      }
           } else {
               completion(nil)
           }
          
       }
    func getAdventureGenre(completion: @escaping (AdventureGenreResponse?) -> Void){
             if Connectivity.isConnectedToInternet() {
                 service.getAdventureGenre(onComplete: {
                            (response) in
                            completion(response)
                        }){
                            
                        }
             } else {
                 completion(nil)
             }
            
         }
    func getCarsGenre(completion: @escaping (CarsGenreResponse?) -> Void){
               if Connectivity.isConnectedToInternet() {
                   service.getCarsGenre(onComplete: {
                              (response) in
                              completion(response)
                          }){
                              
                          }
               } else {
                   completion(nil)
               }
              
           }
    func getDementiaGenre(completion: @escaping (DementiaGenreResponse?) -> Void){
                 if Connectivity.isConnectedToInternet() {
                     service.getDementiaGenre(onComplete: {
                                (response) in
                                completion(response)
                            }){
                                
                            }
                 } else {
                     completion(nil)
                 }
                
             }
    func getDemonsGenre(completion: @escaping (DemonsGenreResponse?) -> Void){
                    if Connectivity.isConnectedToInternet() {
                        service.getDemonsGenre(onComplete: {
                                   (response) in
                                   completion(response)
                               }){
                                   
                               }
                    } else {
                        completion(nil)
                    }
                   
                }
    func getDramaGenre(completion: @escaping (DramaGenreResponse?) -> Void){
                     if Connectivity.isConnectedToInternet() {
                         service.getDramaGenre(onComplete: {
                                    (response) in
                                    completion(response)
                                }){
                                    
                                }
                     } else {
                         completion(nil)
                     }
                    
                 }
    func getComedyGenre(completion: @escaping (ComedyGenreResponse?) -> Void){
                       if Connectivity.isConnectedToInternet() {
                           service.getComedyGenre(onComplete: {
                                      (response) in
                                      completion(response)
                                  }){
                                      
                                  }
                       } else {
                           completion(nil)
                       }
                      
                   }
    func getTopManga(completion: @escaping (TopResponse?) -> Void){
                        if Connectivity.isConnectedToInternet() {
                            service.getTopManga(onComplete: {
                                       (response) in
                                       completion(response)
                                   }){
                                       
                                   }
                        } else {
                            completion(nil)
                        }
                       
                    }
}
