//
//  TopViewController.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
class TopViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource{

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    var mangas = [TopManga]()
    private var viewmodel = ViewModel(repo: Repository(remoteDS: RemoteDataSource(service: ApiManager()), localDS: LocalDataSource()))
         
     override func viewDidLoad() {
         super.viewDidLoad()
         viewmodel.loadTopManga(completion: {
                             (response) in
                 self.mangas = response
                 self.collectionView.reloadData()
             })
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
     }
      func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mangas.count
              }
              
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopCell", for: indexPath) as! TopCollectionViewCell
           cell.poster.layer.cornerRadius = 20
           cell.titlelabel.layer.cornerRadius = 15
           cell.namelabel.text = mangas[indexPath.row].title
           cell.titlelabel.text = String(mangas[indexPath.row].score)
           cell.poster.af.setImage(withURL: (URL(string: "\(mangas[indexPath.row].image )") ?? nil)!)
           return cell
      }
 
    @IBAction func openSearch(_ sender: Any) {
        let id = "SearchManga"
        let FAVViewController = storyboard?.instantiateViewController(identifier: id)
        as? SearchMangaViewController
        view.window?.rootViewController = FAVViewController
        view.window?.makeKeyAndVisible()
    }
    
    @IBAction func backHome(_ sender: Any) {
         let id = "HomePage"
         let FAVViewController = storyboard?.instantiateViewController(identifier: id)
         as? ViewController
         view.window?.rootViewController = FAVViewController
         view.window?.makeKeyAndVisible()
    }
    
}
