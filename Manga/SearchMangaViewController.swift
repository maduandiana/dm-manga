//
//  SearchMangaViewController.swift
//  Manga
//
//  Created by Diana Maduan on 30/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

class SearchMangaViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate  {
   
    private var viewmodel = ViewModel(repo: Repository(remoteDS: RemoteDataSource(service: ApiManager()), localDS: LocalDataSource()))
    var manga = [Manga]()
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var searchbar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
    }
   func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
          viewmodel.loadSearchResults(text:searchbar.text!, completion: {
                                             (response) in
                                 self.manga = response
                                 self.tableview.reloadData()
                             })
                print("tapped")
      }
  
      override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
                 if segue.identifier == "Details" {
                     if let viewController = segue.destination as? DetailsViewController,let index =
                         tableview.indexPathForSelectedRow?.row {
                         viewController.cellValue = manga[index]
                     }
                 }
         }
   override func viewDidAppear(_ animated: Bool) {
              tableview.reloadData()
          }
       public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                 return self.manga.count
            }


      public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "ResultMangaCell", for: indexPath) as! ResultTableViewCell
             cell.title.text = manga[indexPath.row].title
             cell.score.text = String(manga[indexPath.row].score)
             cell.poster.layer.cornerRadius = 20
             cell.poster.af.setImage(withURL: (URL(string: "\(manga[indexPath.row].imageUrl )")!))
             return cell
       }
    @IBAction func backHome(_ sender: Any) {
        let id = "HomePage"
        let FAVViewController = storyboard?.instantiateViewController(identifier: id)
        as? ViewController
        view.window?.rootViewController = FAVViewController
        view.window?.makeKeyAndVisible()
    }
   
}
