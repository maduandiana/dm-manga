//
//  LogViewController.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit
import FirebaseAuth
class LogViewController: UIViewController {

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var errorlabel: UILabel!
    @IBOutlet weak var loginbtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        loginbtn.layer.cornerRadius = 20
        loginbtn.layer.borderWidth = 1
        loginbtn.layer.borderColor = UIColor.purple.cgColor
        password.isSecureTextEntry = true
    }
    
    @IBAction func openSignPage(_ sender: Any) {
        let id = "SignVC"
        let signViewController = storyboard?.instantiateViewController(identifier: id)
        as? SignViewController
        view.window?.rootViewController = signViewController
        view.window?.makeKeyAndVisible()
    }
    
    @IBAction func login(_ sender: Any) {
       let email = username.text!.trimmingCharacters(in: .whitespacesAndNewlines)
       let pass = password.text!.trimmingCharacters(in: .whitespacesAndNewlines)
       Auth.auth().signIn(withEmail: email, password: pass) { (result,error) in
       if error != nil{
           self.errorlabel.text = error!.localizedDescription
           return
       }
       self.transitionToFavouritesPage()
           }
    }
    func transitionToFavouritesPage(){
     let id = "ProfileVC"
     let loginViewController = storyboard?.instantiateViewController(identifier: id) as? ProfileViewController
     view.window?.rootViewController = loginViewController
     view.window?.makeKeyAndVisible()
    }
}
