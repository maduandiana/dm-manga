//
//  ViewModel.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

class ViewModel: NSObject {
   var mangas = [Manga]()
   private var repository: Repository!
   init(repo : Repository) {
       self.repository = repo
   }
    
  
    func loadSearchResults(text:String,completion: @escaping ([Manga]) -> Void){
        repository.getSearchResults(text:text,completion:
                { (response) in
                    completion(response.results)
            })
        }
    func loadCharacters(id:Int,completion: @escaping ([Actors]) -> Void){
        repository.getCharacters(id:id,onComplete:
            { (response) in
                completion(response.characters)
        })
    }
    func loadRecommendations(id:Int,completion: @escaping ([Recommendations]) -> Void){
          repository.getRecommendations(id:id,onComplete:
              { (response) in
                  completion(response.recommendations)
          })
      }
   func loadActionGenre(completion: @escaping ([Manga]) -> Void){
       repository.getActionGenre(onComplete:
           { (response) in
               completion(response.manga)
       })
   }
    func loadAdventureGenre(completion: @escaping ([Manga]) -> Void){
        repository.getAdventureGenre(onComplete:
            { (response) in
                completion(response.manga)
        })
    }
    func loadCarsGenre(completion: @escaping ([Manga]) -> Void){
        repository.getCarsGenre(onComplete:
            { (response) in
                completion(response.manga)
        })
    }
    func loadDementiaGenre(completion: @escaping ([Manga]) -> Void){
        repository.getDementiaGenre(onComplete:
            { (response) in
                completion(response.manga)
        })
    }
    func loadDemonsGenre(completion: @escaping ([Manga]) -> Void){
        repository.getDemonsGenre(onComplete:
            { (response) in
                completion(response.manga)
        })
    }
    func loadDramaGenre(completion: @escaping ([Manga]) -> Void){
        repository.getDramaGenre(onComplete:
            { (response) in
                completion(response.manga)
        })
    }
    func loadComedyGenre(completion: @escaping ([Manga]) -> Void){
        repository.getComedyGenre(onComplete:
            { (response) in
                completion(response.manga)
        })
    }
    func loadTopManga(completion: @escaping ([TopManga]) -> Void){
        repository.getTopManga(onComplete:
            { (response) in
                completion(response.top)
        })
    }
}
