//
//  MangaCollectionViewCell.swift
//  Manga
//
//  Created by Diana Maduan on 29/4/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
class MangaCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titletext: UITextView!
    @IBOutlet weak var poster: UIImageView!
}
