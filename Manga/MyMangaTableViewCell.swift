//
//  MyMangaTableViewCell.swift
//  Manga
//
//  Created by Diana Maduan on 8/5/20.
//  Copyright © 2020 Diana Maduan. All rights reserved.
//

import UIKit

class MyMangaTableViewCell: UITableViewCell {

    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var poster: UIImageView!
    
    @IBOutlet weak var scorelabel: UILabel!
}
