//
//  Mangas+CoreDataProperties.swift
//  
//
//  Created by Diana Maduan on 9/5/20.
//
//

import Foundation
import CoreData


extension Mangas {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Mangas> {
        return NSFetchRequest<Mangas>(entityName: "Mangas")
    }

    @NSManaged public var id: Int16
    @NSManaged public var user: String?
    @NSManaged public var title: String?
    @NSManaged public var synopsis: String?
    @NSManaged public var members: Int16
    @NSManaged public var score: Float
    @NSManaged public var image: String?

}
