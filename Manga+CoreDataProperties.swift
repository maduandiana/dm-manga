//
//  Manga+CoreDataProperties.swift
//  
//
//  Created by Diana Maduan on 9/5/20.
//
//

import Foundation
import CoreData


extension Manga {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Manga> {
        return NSFetchRequest<Manga>(entityName: "Manga")
    }

    @NSManaged public var id: Int16
    @NSManaged public var title: String?
    @NSManaged public var user: String?
    @NSManaged public var score: Float
    @NSManaged public var members: Int16
    @NSManaged public var synopsis: String?
    @NSManaged public var image: String?

}
